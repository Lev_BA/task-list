package routers

import (
	"github.com/astaxie/beego"
	"tasklist/controllers"
)

func init() {
	// Home
	beego.Router("/", &controllers.MainController{})
	beego.Router("/login", &controllers.AuthController{})
	beego.Router("/:id([0-9]+)", &controllers.TaskController{}, "get:LoadUser")
	beego.Router(`/:id([0-9]+)/create-task`, &controllers.TaskController{}, "post:CreateTask")
	beego.Router(`/:id([0-9]+)/delete-task`, &controllers.TaskController{}, "get:DeleteTask")
}
