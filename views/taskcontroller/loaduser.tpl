<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <link rel="stylesheet" href="static/css/style.css">
</head>
<body>
<div class="main">
  <div class="main__wrapper wrapper">
    <h1 class="main__title h h--1">Task maneger</h1>
    <div class="main__tasker tasker">
      <form class="task-form" action="1/create-task" method="post">
        <input type="textarea" class="new-task__field new-task__field--title" name="title-task" id="title-task" placeholder="Название задачи"/>
        <input type="textarea" class="new-task__field new-task__field--desc" name="description-task" id="description-task" placeholder="Описание задача"/>
        <button class="tasker__add-btn" type="submit">Добавить</button>
      </form>
      <ul class="tasker__list">
        {{ range .TaskCollection }}
        <li class="tasker__item task">
          <p class="task__text">{{ .TitleTask }}</p>
          <div class="task__info">
            <p class="task__date">{{ .DescriptionTask }}</p>
            <div class="task__control">
              <a href="/{{ .UserID }}/delete-task?{{ .ID }}" class="task__delete-btn btn">Delete</a>
              <button class="task__done-btn btn">Done</button>
            </div>
          </div>
        </li>
        {{ end }}
      </ul>
    </div>
  </div>
</div>
<script src="static/js/script.js"></script>
</body>
</html>