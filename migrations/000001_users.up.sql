create table users
(id bigserial constraint users_pk primary key,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    email      varchar,
    password   varchar,
    active     boolean,
    first_name varchar,
    last_name  varchar
);

create unique index users_id_uindex on users (id);