create table task_list
(
    id bigserial constraint task_list_pk primary key,
    user_id integer REFERENCES users (id),
    created_at       timestamp with time zone,
    updated_at       timestamp with time zone,
    title_task       varchar not null,
    description_task varchar,
    active           boolean not null
);
create unique index task_list_id_uindex on task_list (id);