package controllers

import (
	"github.com/astaxie/beego"
	"gorm.io/gorm"
	"tasklist/models"
)

var DB *gorm.DB

type BaseController struct {
	beego.Controller
	User models.User
}

func (c *BaseController) Get() {

}
