package controllers

import (
	"fmt"
	"strconv"
	"strings"
	"tasklist/models"
)

type TaskController struct {
	BaseController
}

func (c *TaskController) LoadUser() {
	//получаем номер пользователя
	idUser := c.Ctx.Input.Param(":id")

	//получаем таски по номеру пользователя
	var tasks []models.Task
	err := DB.Where("user_id = ?", idUser).Order("created_at desc").Find(&tasks).Error
	if err != nil {
		fmt.Println(err)
	}

	//выгружаем их на страницу
	c.Data["TaskCollection"] = tasks
}

func (c *TaskController) CreateTask() {
	var task models.Task

	//получаем из урла ид пользователя и преобразуем его в uint64
	userID, err := getUserID(c.Ctx.Request.RequestURI)
	if err != nil {
		fmt.Println(err)
	}
	//заполняем информацию о таске данными, полученные с формы
	task.UserID = userID
	task.TitleTask = c.Ctx.Request.FormValue("title-task")
	task.DescriptionTask = c.Ctx.Request.FormValue("description-task")
	task.Active = true

	//создаем таск
	err = DB.Create(&task).Error
	if err != nil {
		fmt.Errorf("%v", err)
	}

	//создаем путь редиректа
	redirectPath := fmt.Sprint("/%s", userID)

	//редиректим
	c.Redirect(redirectPath, 302)
}

func (c *TaskController) DeleteTask() {
	var task models.Task

	//из параметров получаем ид удаляемого таска
	deleteID := c.Ctx.Request.URL.RawQuery

	//фильтруем и удаляем таск
	err := DB.Where("id = ?", deleteID).Delete(&task).Error
	if err != nil {
		fmt.Println(err)
	}

	//формируем путь редиректа
	redirectPath := fmt.Sprint("/%s", deleteID)

	//делаем редирект
	c.Redirect(redirectPath, 302)
}

//getUserID достает ид пользователя из урл и преобразует в uint64
func getUserID(path string) (uint64, error) {
	//номер элемента ид в урл
	const numberElement = 1

	userID := strings.Split(path, "/")[numberElement]

	id, err := strconv.ParseUint(userID, 0, 64)
	if err != nil {
		return 0, err
	}

	return id, nil
}
