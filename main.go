package main

import (
	"github.com/astaxie/beego"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"tasklist/controllers"
	_ "tasklist/routers"
)

type Product struct {
	gorm.Model
	Code  string
	Price uint
}

/*type Tasks struct {
	ID   uint
	Date string
	Task string
}*/

func main() {
	var DB *gorm.DB
	var err error
	dsn := "host=localhost user=postgres password=1111 dbname=taskList port=5432 sslmode=disable TimeZone=Asia/Shanghai"
	DB, err = gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		panic("failed to connect database")
	}
	controllers.DB = DB

	beego.Run()
}
