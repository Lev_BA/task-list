package models

import "time"

type Task struct {
	ID              uint64    `gorm:"id"`
	UserID          uint64    `gorm:"user_id"`
	CreatedAt       time.Time `gorm:"created_at"`
	UpdatedAt       time.Time `gorm:"updated_at"`
	TitleTask       string    `gorm:"title_task"`
	DescriptionTask string    `gorm:"description_task"`
	Active          bool      `gorm:"active"`
}
