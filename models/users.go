package models

import "time"

type User struct {
	ID        uint64    `gorm:"id"`
	CreatedAt time.Time `gorm:"created_at"`
	UpdatedAt time.Time `gorm:"updated_at"`
	Email     string    `gorm:"email"`
	Password  string    `gorm:"password"`
	Active    bool      `gorm:"active"`
	FirstName string    `gorm:"first_name"`
	LastName  string    `gorm:"last_name"`
}
